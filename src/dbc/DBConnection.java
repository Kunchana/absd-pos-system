package dbc;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBConnection {
    public static DataSource getConnectionPool(){
        InitialContext context = null;
        DataSource pool = null;

        try {
            context = new InitialContext();
            pool = (DataSource) context.lookup("java:comp/env/pos");
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return pool;

    }
}

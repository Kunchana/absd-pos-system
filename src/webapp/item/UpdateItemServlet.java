package webapp.item;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "UpdateItemServlet")
public class UpdateItemServlet extends HttpServlet {

    DataSource pool;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if (pool == null){
            pool = (DataSource) servletContext.getAttribute("dbpool");
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection connection = null;
        PreparedStatement stm = null;
        String code = request.getParameter("code");
        String name = request.getParameter("description");
        int qty = Integer.parseInt(request.getParameter("qty"));
        double price = Double.parseDouble(request.getParameter("price"));


        try {

            connection = pool.getConnection();

            stm = connection.prepareStatement("UPDATE item set  itemDescription = ?,  itemQuantity = ? , unitPrice = ? where itemModelCode = ? ");
            stm.setString(1,name);
            stm.setInt(2,qty);
            stm.setDouble(3,price);
            stm.setString(4,code);
            int resultSet = stm.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stm != null){
                try {
                    stm.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                };

            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

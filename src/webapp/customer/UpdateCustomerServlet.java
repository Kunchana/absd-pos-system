package webapp.customer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "UpdateCustomerServlet")
public class UpdateCustomerServlet extends HttpServlet {
    DataSource pool;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if (pool == null){
            pool = (DataSource) servletContext.getAttribute("dbpool");
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        Connection connection = null;
        PreparedStatement stm = null;

        try {

            connection = pool.getConnection();

            stm = connection.prepareStatement("UPDATE customer set  customerName = ?,  customerEmail = ? , customerPassword = ? where customerPhone = ? ");
            stm.setString(1,name);
            stm.setString(2,email);
            stm.setString(3,password);
            stm.setString(4,phone);

            int resultSet = stm.executeUpdate();
            System.out.println(resultSet);

        } catch ( SQLException e) {
            e.printStackTrace();
        }finally {
            if (stm != null && connection != null){
                try {
                    stm.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

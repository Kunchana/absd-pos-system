package webapp.customer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "CustomerServlet")
public class CustomerServlet extends HttpServlet {

    DataSource pool;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if (pool == null){
            pool = (DataSource) servletContext.getAttribute("dbpool");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = pool.getConnection();

            statement = connection.prepareStatement("INSERT INTO Customer VALUES(?,?,?,?)");
            statement.setString(1,phone);
            statement.setString(2,name);
            statement.setString(3,email);
            statement.setString(4,password);
            int ResultSet = statement.executeUpdate();
            System.out.println(ResultSet);

        } catch ( SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null & connection != null){
                try {
                    statement.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

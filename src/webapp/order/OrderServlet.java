package webapp.order;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "OrderServlet")
public class OrderServlet extends HttpServlet {
    DataSource pool;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if (pool == null){
            pool = (DataSource) servletContext.getAttribute("dbpool");
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String orderCode = request.getParameter("orderCode");
        String itemModelCode = request.getParameter("itemModelCode");
        String phone = request.getParameter("phone");
        int qty = Integer.parseInt(request.getParameter("qty"));
        double price = Double.parseDouble(request.getParameter("price"));
        String orderDate = request.getParameter("orderDate");

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = pool.getConnection();

            statement = connection.prepareStatement("INSERT INTO orders VALUES(?,?,?)");
            statement.setString(1,orderCode);
            statement.setDouble(2,price);
            statement.setString(3,orderDate);
            int resultSet = statement.executeUpdate();

            int qunatity = 0;
            if (resultSet == 1){
                statement = connection.prepareStatement("select itemQuantity from item where itemModelCode = ? ");
                statement.setString(1,itemModelCode);
                ResultSet set = statement.executeQuery();
                if (set.next()){
                    qunatity = set.getInt(1);
                }
                if (qunatity >= qty){
                    statement = connection.prepareStatement("INSERT INTO orderDetails VALUES(?,?,?,?,?)");
                    statement.setString(1,orderCode);
                    statement.setString(2,itemModelCode);
                    statement.setString(3,phone);
                    statement.setInt(4,qty);
                    statement.setDouble(5,price);
                    int rst = statement.executeUpdate();

                    if (rst == 1){
                        statement = connection.prepareStatement("UPDATE item set itemQuantity = itemQuantity-? WHERE itemModelCode = ? ");
                        statement.setInt(1,qty);
                        statement.setString(2,itemModelCode);
                        int res = statement.executeUpdate();

                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null && connection != null){
                try {
                    connection.close();
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

$(document).ready(
    function () {

        /**
         * house form input fields
         */
        let $customerID = $('#customerID');
        let $customerName = $('#customerName');
        let $customerAddress = $('#customerAddress');
        let $customerSalary = $('#customerSalary');

        /**
         * setup house attributes
         */
        let $customerSaveBtn = $('#customerSaveBtn');
        let $customerTable = $('#customerTable');

        /**
         * this method for set customer row
         *
         * @param id
         * @param name
         * @param address
         * @param salary
         * @returns {string}
         */
        function getCustomer(id, name, address, salary) {

            let customerRow = "<tr>\n" +
                "<th scope=\"row\">" + id + "</th>\n" +
                "<td>" + name + "</td>\n" +
                "<td>" + address + "</td>\n" +
                "<td>" + salary + "</td>\n" +
                "</tr>";

            return customerRow;
        }

        /**
         * this method for analyse the data form response
         *
         * @param data
         */
        function analyzeResponse(data) {
            for (let i in data) {
                let houseData = data[i];

                let id = houseData[0];
                let name = houseData[1];
                let hPoint = houseData[2];
                let icon = houseData[3];

                $customerTable.append(getCustomer(id, name, icon, hPoint));
            }
        }

        /**
         * this method for load all custoemrs
         */
        function loadAllCustomers() {
            $.ajax({
                type: "GET",
                url: baseURL + '/House/all',
                dataType: 'json'
            }).done(function (data) {
                analyzeResponse(data);
            });
        }

        // loadAllCustomers();

        /**
         * this method for ajax request to save customer
         */
        function saveCustomer() {
            $.ajax({
                url: 'http://localhost:8080/ThogakadeProject_war_exploded/customer',
                type: "POST",
                data: {
                    code: $customerID.val(),
                    name: $customerName.val(),
                    address: $customerAddress.val(),
                    salary: $customerSalary.val()
                },
                dataType: 'json'
            }).done(function (data) {
                alert(data);

                loadAllCustomers();
            });
        }

        $customerSaveBtn.click(function () {
            saveCustomer();
        });

    });

